import Glide from '@glidejs/glide';
import lottie from 'lottie-web';
import Loader from './loader';

const containerClass = '.entry-slider';
const activeSlideClass = '.glide__slide--active';
const activeDotClass = '.glide__bullet--active';

let currentInterval = null;
let isPaused = false;
let n = 1;
const lottieInstances = [];

// Create Slider Nav Elements
const generateDots = (glideContainer) => {
  const elementsCount = glideContainer.querySelectorAll('.glide__slide').length;
  const dotsWrap = document.createElement('div');
  dotsWrap.className = 'glide__bullets';
  dotsWrap.setAttribute('data-glide-el', 'controls[nav]');

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < elementsCount; i++) {
    const dotProgress = document.createElement('div');
    dotProgress.className = 'dot-progress';

    const dotWrap = document.createElement('div');
    dotWrap.className = 'dot-wrap';
    dotWrap.appendChild(dotProgress);

    const dot = document.createElement('button');
    dot.className = 'glide__bullet';
    dot.setAttribute('data-glide-dir', `=${i}`);
    dot.appendChild(dotWrap);

    dotsWrap.appendChild(dot);
  }

  glideContainer.appendChild(dotsWrap);
};

// Animate Background On Slider Hover
const hoverBgAnimate = (index) => {
  const el = document.querySelector(activeSlideClass);
  el.onmousemove = (e) => {
    const slideBG = el.querySelector('.slide__bg');
    const mouseX = e.clientX / (window.innerWidth / 7.5);
    slideBG.style.transform = `translate(${-mouseX}%, -5%)`;

    if (lottieInstances[index].isPaused) {
      lottieInstances[index].stop();
    }
    lottieInstances[index].play();
  };
};

// Progress Bar On Active Nav
const bulletProgress = () => {
  clearInterval(currentInterval);
  currentInterval = null;
  isPaused = false;
  n = 1;

  const sliderNavList = document.querySelectorAll('.dot-progress');
  sliderNavList.forEach((item) => {
    item.style.width = 0;
  });

  currentInterval = setInterval(() => {
    const progressInActive = document.querySelector(`${activeDotClass} .dot-progress`);
    if (!isPaused) {
      n += 1;
      if (progressInActive.style.width === '' || parseInt(progressInActive.style.width, 10) < n) {
        progressInActive.style.width = `${n}%`;
      }
    }
  }, 50);
};

// Pause Slider On Hover To Exact Elements
const pauseOnContent = (glideSlider) => {
  const sliderControl = (el) => {
    el.onmouseover = () => {
      isPaused = true;
      glideSlider.pause();
    };
    el.onmouseout = () => {
      isPaused = false;
      n = 1;
      glideSlider.play();
    };
  };
  const activeSlideText = document.querySelector(`${activeSlideClass} .slide__text`);
  const activeSlideLottie = document.querySelector(`${activeSlideClass} .lottie__content`);
  sliderControl(activeSlideText);
  sliderControl(activeSlideLottie);
};

// Home Page Big Slider
const entrySlider = () => {
  const glideContainer = document.querySelector(containerClass);

  if (glideContainer) {
    // Slider Configs
    const glideSlider = new Glide(glideContainer, {
      type: 'carousel',
      gap: 0,
      autoplay: 5000,
      animationTimingFunc: 'ease-out',
      animationDuration: 300,
      hoverpause: false,
    });

    // Initializing Lottie Animation
    const lotties = document.querySelectorAll('.lottie__area');
    let lottieCount = 0;
    lotties.forEach((item) => {
      const lottieEl = lottie.loadAnimation({
        container: item.querySelector('.lottie__content'),
        renderer: 'svg',
        loop: false,
        animationData: JSON.parse(item.getAttribute('data-lottie')),
        rendererSettings: {
          progressiveLoad: true,
        },
      });
      setTimeout(() => {
        lottieEl.setSpeed(0.5);
      }, 250);
      lottieInstances.push(lottieEl);

      lottieEl.addEventListener('loaded_images', () => {
        lottieCount += 1;
        
        if (lottieCount === lotties.length) {
          // Slider Init
          glideSlider.on('mount.before', () => generateDots(glideContainer));
          glideSlider.on(['mount.after', 'run.after'], () => {
            const activeLottie = document.querySelector(`${activeSlideClass} .lottie__area`);
            const activeLottieIndex = Number(activeLottie.getAttribute('data-index'));
            setTimeout(() => {
              if (lottieInstances[activeLottieIndex].isPaused) {
                lottieInstances[activeLottieIndex].stop();
              }
              lottieInstances[activeLottieIndex].play();
            }, 10);

            hoverBgAnimate(activeLottieIndex);
            bulletProgress();
            pauseOnContent(glideSlider);
          });
          // Hide Loader
          Loader.hideLoader();
          glideSlider.mount();
        }
      });
    });
  }
};
export default entrySlider;
