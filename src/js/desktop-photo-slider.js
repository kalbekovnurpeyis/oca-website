import { tns } from 'tiny-slider/src/tiny-slider';
import OverlayScrollbars from 'overlayscrollbars';

const defaultOptions = {
  autoWidth: true,
  items: 1,
  center: true,
  mouseDrag: true,
  speed: 1000,
  controlsText: [
    '<svg width="12" height="19" viewBox="0 0 12 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.8296 17.8686L2.55273 9.59175L10.8296 1.24295" stroke="#000E26" stroke-opacity="0.5" stroke-width="3" stroke-miterlimit="10"/></svg>',
    '<svg width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.92239 1.88306L10.1992 10.1599L1.92239 18.5087" stroke="#000E26" stroke-opacity="0.5" stroke-width="3" stroke-miterlimit="10"/></svg>',
  ],
  touch: true,
  preventScrollOnTouch: 'auto',
  navAsThumbnails: true,
};

export function createDesktopPhotoSlider({
  container = '.desktop-photo-slider__container',
  navContainer = '.desktop-photo-slider__thumbnails',
  options = defaultOptions,
} = {}) {
  const scrollbarContainer =
    navContainer instanceof HTMLElement
      ? navContainer
      : document.querySelector(navContainer);
  const scrollbar = OverlayScrollbars(scrollbarContainer.parentNode, {className: 'os-theme-round-light' });

  const slider = tns({
    container,
    navContainer,
    ...options,
  });

  slider.events.on('transitionStart', () =>
    scrollbar.scroll(
      {
        el: document.querySelector(
          '.desktop-photo-slider__thumbnail.tns-nav-active'
        ),
        block: 'center',
      },
      300
    )
  );

  return slider;
}
