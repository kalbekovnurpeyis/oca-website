import { tns } from 'tiny-slider/src/tiny-slider';

// Gallery Slider
const sportsListMobileSlider = () => {

  const slider = tns({
    container: '.sports-list-mobile-slider',
    autoWidth: true,
    loop: false,
    items: 5,
    navAsThumbnails: false,
    mouseDrag: true,
    controls: false,
    touch: true,
    preventScrollOnTouch: 'auto',
 });
}

export default sportsListMobileSlider;
