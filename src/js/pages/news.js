import photoSlider from '../photo-slider';

document.addEventListener('DOMContentLoaded', () => {
  // Photo Slider Initialization
  photoSlider();
});
