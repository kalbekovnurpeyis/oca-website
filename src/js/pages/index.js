import { tns } from "tiny-slider/src/tiny-slider";
import photoSlider from "../photo-slider";
import entrySlider from "../entry-slider";
import Loader from "../loader";

// Show Loader
Loader.showLoader();

document.addEventListener("DOMContentLoaded", () => {
  // Big Slider Initialization
  entrySlider();

  // Banner slider
  const bannerSliderClass = '.banner-slider__wrap';
  if (document.querySelector(bannerSliderClass)) {
    tns({
      container: bannerSliderClass,
      items: 1,
      mouseDrag: true,
      speed: 1000,
      controlsText: [
        '<div class="banner-arrow banner--prev"></div>',
        '<div class="banner-arrow banner--next"></div>'
      ],
      controls: true,
      loop: true,
      nav: false,
      touch: true,
      lazyload: true,
    });
  }

  const timelineContainers = [
    "game-timeline",
    "event-timeline"
  ];
  timelineContainers.map(el => {
    if (document.querySelector(`.${el} .timeline-slider`)) {
      tns({
        container: `.${el} .timeline-slider`,
        items: 2,
        mouseDrag: true,
        slideBy: "page",
        speed: 1000,
        controlsText: [
          '<div class="timeline-arrow timeline--prev"></div>',
          '<div class="timeline-arrow timeline--next"></div>'
        ],
        controls: true,
        loop: false,
        nav: false,
        touch: true,
        lazyload: true,
        responsive: {
          426: {
            items: 4,
            slideBy: "page"
          }
        }
      }).goTo("last");
    }
  });

  const sponsorsContainers = [1, 2];
  sponsorsContainers.map(el => {
    if (document.querySelector(`.sopnsors-list__slider-${el}`)) {
      tns({
        container: `.sopnsors-list__slider-${el}`,
        items: 2,
        mouseDrag: true,
        slideBy: "page",
        speed: 1000,
        controlsText: [
          '<div class="sponsor-arrow sponsor--prev"></div>',
          '<div class="sponsor-arrow sponsor--next"></div>'
        ],
        controls: true,
        loop: false,
        nav: false,
        touch: true,
        lazyload: true,
        responsive: {
          426: {
            items: 3,
            slideBy: "page"
          },
          769: {
            items: 4,
            slideBy: "page"
          },
          1025: {
            items: 5,
            slideBy: "page"
          }
        }
      });
    }
  });

  // Photo Slider Initialization
  photoSlider();
});
