import AOS from 'aos';
import 'core-js/stable';
import 'regenerator-runtime/runtime';

document.addEventListener('DOMContentLoaded', () => {
  // Appearance Animation Library Initialization
  AOS.init();

  // Navigation Scripts
  const body = document.querySelector('.body');
  const navigation = document.querySelector('.navigation');
  const navigationClose = navigation.querySelector('.navigation__close');
  const navigationBackground = document.querySelector('.navigation__background');
  const navItems = document.querySelectorAll('.nav__item');
  const navLinks = document.querySelectorAll('.nav__link');
  const navToggle = document.querySelector('.header .nav-toggle');

  document.addEventListener('keyup', e => {
    if (
      (e.code === 'Escape' || e.keyCode === 27) &&
      navigation.classList.contains('navigation--active')
    ) {
      toggleNavigationOpen(false);
    }
  });

  navigationClose.addEventListener('click', () => toggleNavigationOpen(false));

  navToggle.addEventListener('click', e => {
    // e.preventDefault();
    const shouldOpen = !navToggle.classList.contains('nav-toggle--active');

    toggleNavigationOpen(shouldOpen);
  });

  navLinks.forEach(item => {
    item.onmouseover = () => {
      item.classList.add('nav__link--hovered');
      navigationBackground.style.backgroundImage = `url(${item.getAttribute('data-bg')})`;
      setTimeout(() => {
        navigationBackground.classList.add('navigation__background--hovered');
      }, 0.1);
    };
    item.onmouseout = () => {
      navigationBackground.classList.remove('navigation__background--hovered');
      item.classList.remove('nav__link--hovered');
      navigationBackground.style.backgroundImage = 'none';
    };
  });

  function removeNavLinkAnimation(isMenuActive) {
    navItems.forEach((item, index) => {
      item.style.transitionDelay = `${
        isMenuActive ? 0 : (6 + index / 2) / 10
      }s`;
    });
  }

  function toggleNavigationOpen(open) {
    const method = open ? 'add' : 'remove';
    body.classList[method]('block-scroll');
    navToggle.classList[method]('nav-toggle--active');
    navigation.classList[method]('navigation--active');
    removeNavLinkAnimation(!open);
  }
});
