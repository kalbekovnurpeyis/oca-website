import Glide from '@glidejs/glide';
import OverlayScrollbars from 'overlayscrollbars';

document.addEventListener('DOMContentLoaded', () => {
  // Scrollbar Library
  const scrollElement = document.querySelector('.games__categories');
  const scrollBar = OverlayScrollbars(scrollElement, {
    className: 'os-theme-round-light',
  });
  // Scroll List To Active Element
  scrollBar.scroll({
    el: document.querySelector('.games__category-link--active'),
    block: 'center',
  });

  const mainSliderWrap = document.querySelector('.timeline__main-slider');
  const navSliderWrap = document.querySelector('.timeline__nav-slider');

  if (mainSliderWrap && navSliderWrap) {
    // Sliders Configs
    const mainSlider = new Glide(mainSliderWrap, {
      gap: 0,
      animationTimingFunc: 'ease-in',
      direction: 'rtl',
      rewind: false,
    });
    const navSlider = new Glide(navSliderWrap, {
      animationTimingFunc: 'ease-in',
      direction: 'rtl',
      rewind: false,
      perView: 5,
      focusAt: 'center',
      breakpoints: {
        768: {
          perView: 3,
        },
      },
    });

    // Sync 2 Sliders
    mainSlider.on('run.before', () => {
      setTimeout(() => {
        navSlider.go(`=${mainSlider.index}`);
      }, 1);
    });
    navSlider.on('run.before', () => setTimeout(() => {
      mainSlider.go(`=${navSlider.index}`);
    }, 1));

    // nav slide on click event
    const navDots = document.querySelectorAll('.slide__content');
    for (let i = 0; i < navDots.length; i++) {
      const navSlideDot = navDots[i].querySelector('.slide__dot');
      if (navSlideDot) {
        navDots[i].onclick = () => {
          const goToIndex = navDots[i].getAttribute('slide-index');
          navSlider.go(`=${goToIndex}`);
        };
      }
    }

    // Init Sliders
    mainSlider.mount();
    navSlider.mount();
    // mainSlider.go('>>');
    // navSlider.go('>>');
  }
});
