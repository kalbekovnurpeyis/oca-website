import countryListFallback from "../countries.json";

window.onload = function onload() {
  const countries = window.countries || countryListFallback;
  const map = document.querySelector("#map");
  const countryItems = document.querySelectorAll(".country-list__item");
  const countryInfoBlock = document.querySelector(".country-info__item");

  Array.from(map.getElementsByTagName("path")).forEach(el => {
    if (!countries.hasOwnProperty(el.id)) {
      el.className.baseVal = "inactive";
    }
  });

  let prevCountry = null;

  map.addEventListener("click", displayCountry);

  function displayCountry(e) {
    if (e.target.tagName !== "path") {
      return;
    }

    const country = e.target;
    const { id: countryId } = country;
    const inactive = country.className.baseVal === "inactive";

    if (inactive) {
      return;
    }

    if (prevCountry) {
      clearPreviousCountry(prevCountry, countryItems);
    }

    const countryData = countries[countryId];
    prevCountry = country;
    country.className.baseVal = "selected";

    highlightItems(countryItems, countryId);

    if (!countryData) {
      return;
    }

    countryInfoBlock.innerHTML = renderCountryInfo(countryData);
  }

  /**
   *
   * @param {HTMLCollectionOf<HTMLUListElement>} list
   * @param {string} selectedCountry
   * @param {Boolean}
   */
  function highlightItems(list, selectedCountry, remove = false) {
    Array.from(list).forEach(item => {
      if (item.dataset.countryId === selectedCountry) {
        const method = remove ? "remove" : "add";
        item.classList[method]("highlighted");
      }
    });
  }

  function clearPreviousCountry(country, items) {
    if (country) {
      highlightItems(items, country.id, true);

      if (country.classList) {
        country.classList.remove("selected");
      }

      if (country.hasAttribute("class")) {
        country.removeAttribute("class");
      }

      countryInfoBlock.innerHTML = "";
    }
  }
};

function renderCountryInfo(country) {
  return `
    <h2 class="country-info__title">
      ${country.name}
      ${
        country.country_flag
          ? `
      <div class="country-info__flag">
        <img src="${country.country_flag}" alt="COUNTRY FLAG">
      </div>
      `
          : ""
      }
    </h2>

    ${
      country.committee_name
        ? `<div class="committee-name">${country.committee_name}</div>`
        : ""
    }

    ${
      country.committee_president_name && country.committee_president_url
        ? `
    <div class="info__content">
      <div class="info__content-label">President</div>
      <div class="info__content-value">
        <a href="${country.committee_president_url}">${country.committee_president_name}</a>
      </div>
    </div>
    `
        : ""
    }
    
    ${
      country.phone || country.fax
        ? `
    <div class="info__content">
      <div class="info__content-label">Contact details</div>
      <div class="info__content-value">
        ${
          country.phone
            ? `
          <div><a href="tel:${country.phone}">Tel: ${country.phone}</a></div>
        `
            : ""
        }
        ${
          country.fax
            ? `
          <div><a href="fax:${country.fax}">Fax: ${country.fax}</a></div>
        `
            : ""
        }
      </div>
    </div>
    `
        : ""
    }

    ${
      country.website
        ? `
    <div class="info__content">
      <div class="info__content-label">Web address</div>
      <div class="info__content-value">
        <a href="${getCountryUrl(country)}" target="_blank">${
            country.website
          }</a>
      </div>
    </div>
    `
        : ""
    }

    ${
      country.email
        ? `
    <div class="info__content">
      <div class="info__content-label">Email</div>
      <div class="info__content-value">
        <a href="mailto:${country.email}">${country.email}</a>
      </div>
    </div>
    `
        : ""
    }
    
    ${
      country.committee_flag
        ? `
    <div class="info__content">
      <div class="info__content-label">Committee flag</div>
      <div class="info__content-value">
        <div class="info__img-wrap">
          <img src="${country.committee_flag}" alt="COUNTRY FLAG">
        </div>
      </div>
    </div>
    `
        : ""
    }
  `;
}

function getCountryUrl(country) {
  return `${
    country.website && country.website.includes("http") ? "" : "http://"
  }${country.website || "#"}`;
}
