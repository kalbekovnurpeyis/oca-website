// import { tns } from 'tiny-slider/src/tiny-slider';

// import leftArrow from '../../images/left-arrow.svg';
// import rightArrow from '../../images/right-arrow.svg';

// document.addEventListener('DOMContentLoaded', () => {
//   const logo = document.querySelector('.header__logo');
//   const menuText = document.querySelector('.nav-toggle__text');
//   menuText.style.display = 'none';
//   if (window.matchMedia('(min-width: 426px)').matches) {
//     logo.style.visibility = 'hidden';
//   } else {
//     logo.style.visibility = 'visible';
//   }
//   const innerSliderWrap = document.querySelector('.games-timeline-slider');
//   let firstSlider;
//   let secondSlider;
//   if (innerSliderWrap) {
//     firstSlider = tns({
//       container: '.games-timeline-slider',
//       navContainer: '.timeline-nav',
//       navAsThumbnails: true,
//       items: 1,
//       preventActionWhenRunning: true,
//       mouseDrag: false,
//       animateIn: 'rollIn',
//       animateOut: 'rollOut',
//       mode: 'gallery',
//       slideBy: 'page',
//       autoplay: true,
//       speed: 700,
//       controlsText: [
//         `<img src="${leftArrow}" title="Prev" alt="Previous" />`,
//         `<img src="${rightArrow}" title="Next" alt="Next" />`,
//       ],
//       controls: true,
//       loop: true,
//       nav: true,
//       touch: true,
//     });

//     secondSlider = tns({
//       container: '.timeline-nav',
//       items: 5,
//       mouseDrag: true,
//       center: true,
//       autoplay: true,
//       speed: 700,
//       controls: false,
//       nav: false,
//       touch: true,
//       preventScrollOnTouch: 'auto',
//     });


//     const navItems = document.querySelectorAll('.timeline-nav__item');
//     navItems.forEach((navItem) => {
//       navItem.addEventListener('click', changeSlide);
//     });


//     let currentIndex = null;


//     const firstNavItem = document.querySelector('[data-nav="0"');
//     firstNavItem.className = 'timeline-nav__item tns-item tns-nav-active tns-slide-active';
//    setTimeout(() => {
//     const currentNavs = document.querySelectorAll(`[data-nav="0"]`);
//     currentNavs.forEach(navItem => {
//       navItem.children[0].style.transform = 'translate(750px, -450px)';
//     })
//    }, 0)

//     let currentTarget = null;
//     function changeSlide(event) {
//       console.log('change slide');
//       currentTarget = event.currentTarget;
   
//       const currentNavs = document.querySelectorAll(`[data-nav="${event.currentTarget.dataset.nav}"]`)
     
//       navItems.forEach(navItem => {
//         navItem.classList.remove('tns-nav-active');
//         navItem.classList.remove('tns-slide-active');
//         navItem.className = 'timeline-nav__item tns-item';
//         navItem.children[0].style.transform = 'translate(0, 0)';
//       });

//       const allNavs = document.querySelectorAll(`[data-nav]`);
//       allNavs.forEach(navItem => {
//         navItem.children[0].style.transform = 'translate(0, 0)';
//       })

//       currentNavs.forEach(navItem => {

//           navItem.classList.remove('tns-nav-active');
//           navItem.classList.remove('tns-slide-active');
//           navItem.className = 'timeline-nav__item tns-item tns-nav-active tns-slide-active';
//           navItem.children[0].style.transform = 'translate(0, 0)';
       
//       });
    
//       secondSlider.goTo(event.currentTarget.dataset.nav);
//       // currentIndex = event.currentTarget.dataset.nav;
//       // firstSlider.refresh();
//       // secondSlider.refresh();
//     setTimeout(() => {
//       currentTarget.className = 'timeline-nav__item tns-item tns-nav-active tns-slide-active';
//       currentTarget.children[0].style.transform = 'translate(760px, -450px)';

//       currentNavs.forEach(navItem => {

//         navItem.className = 'timeline-nav__item tns-item tns-nav-active tns-slide-active';
//         navItem.children[0].style.transform = 'translate(760px, -450px)';
     
//     });
//     }, 0)

//     }


//       secondSlider.events.on('indexChanged', indexChanged);
    
//    currentIndex = currentIndex || 1;
//    let secondSliderIndex = 1;
//     let navCount = document.querySelectorAll('[data-nav]').length;
//     console.log('navCount', navCount);
//     let gamesItemsLength = document.querySelector('.games-timeline-slider').children.length;
//     console.log('gamesItemsLength', gamesItemsLength);
//       function indexChanged() {
//         console.log('after manual set', currentIndex);
//         console.log('secondSliderIndex', secondSliderIndex);
//         secondSliderIndex++;
//         // console.log(currentIndex);
//         // const allNavs = document.querySelectorAll(`[data-nav]`);
//         // allNavs.forEach(navItem => {
//         //   navItem.children[0].style.transform = 'translate(0, 0)';
//         // })
//         navItems.forEach(navItem => {
//           navItem.classList.remove('tns-nav-active');
//           navItem.classList.remove('tns-slide-active');
//           navItem.className = 'timeline-nav__item tns-item';
//           navItem.children[0].style.transform = 'translate(0, 0)';
//         });
//         if (currentIndex === 6) {
//           currentIndex = 0;
//           // return;
//          //  if (secondSlider.getInfo().index === navCount) {
//            //  secondSlider.goTo(0);
           
//          //  }
//            // secondSlider.goTo(0);
//         }
//          if (currentIndex < 6) {
//           // if (currentIndex < navItems) {
        
//           const currentNavs = document.querySelectorAll(`[data-nav="${currentIndex}"]`)
    
//           currentNavs.forEach(navItem => {
//             navItem.className = 'timeline-nav__item tns-item tns-nav-active tns-slide-active';
//             navItem.children[0].style.transform = 'translate(760px, -450px)';
         
//         });
//           currentIndex++;
//           return;
//          }

//         // if (currentIndex)

//       }

    

//   }



  

// });






import Glide from '@glidejs/glide';

document.addEventListener('DOMContentLoaded', () => {
  const mainSliderWrap = document.querySelector('.timeline__main-slider');
  const navSliderWrap = document.querySelector('.timeline__nav-slider');

  if (mainSliderWrap && navSliderWrap) {
    const mainSlider = new Glide(mainSliderWrap, {
      type: 'carousel',
      gap: 0,
      animationTimingFunc: 'ease-out',
      rewind: false,
      animationDuration: 400,
      direction: 'rtl',
      // autoplay: 3000,
    });
    mainSlider.mount();

    const navSlider = new Glide(navSliderWrap, {
      type: 'carousel',
      animationTimingFunc: 'ease-out',
      rewind: false,
      perView: 5,
      animationDuration: 400,
      focusAt: 0,
    });
    navSlider.mount();

    // Sync 2 Sliders
    mainSlider.on(['run.before'], () => setTimeout(() => {
      navSlider.go(`=${mainSlider.index}`);
    }, 1));

    navSlider.on(['run.before'], () => setTimeout(() => {
      mainSlider.go(`=${navSlider.index}`);
    }, 1));

    // nav slide on click event
    const navDots = document.querySelectorAll('.slide__content');
    for (let i = 0; i < navDots.length; i++) {
      navDots[i].onclick = () => {
        const goToIndex = navDots[i].closest('.glide__slide').getAttribute('slide-index');
        navSlider.go(`=${goToIndex}`);
      };
    }
  }
});
