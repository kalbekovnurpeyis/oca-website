document.addEventListener('DOMContentLoaded', () => {
  const countries = document.querySelectorAll('.medal-table__country');
  const sportsContainers = document.querySelectorAll('.medal-table__sports');
  const sports = document.querySelectorAll('.medal-table__sport-wrap');
  const places = document.querySelectorAll('.medal-table__places');

  function closeAllSports() {
    if (sportsContainers) {
      Object.keys(sportsContainers).map(function (i) {
        sportsContainers[i].classList.remove('shown');
      });
    }
  }

  function closeAllPlaces() {
    if (places) {
      Object.keys(places).map(function (i) {
        places[i].classList.remove('shown');
      });
    }
  }

  function toggleLogic(instance, type) {
    if (instance.classList.contains('shown')) {
      instance.classList.remove('shown');
    } else {
      if (type === 'sport') {
        closeAllSports();
      }
      closeAllPlaces();

      instance.classList.add('shown');
    }
  }

  if (countries) {
    Object.keys(countries).map(function (i) {
      const country = countries[i];
      const constryTotals = country.querySelector('.medal-table__country-totals');

      if (constryTotals) {
        constryTotals.onclick = function () {
          toggleLogic(country.querySelector('.medal-table__sports'), 'sport');
        };
      }
    });
  }

  Object.keys(sports).map(function(i) {
    const sport = sports[i];
    const sportTotals = sport.querySelector('.medal-table__sport-wrap__totals');

    if (sportTotals) {
      sportTotals.onclick = function() {
        toggleLogic(sport.querySelector('.medal-table__places'), 'place');
      };
    }
  });
});