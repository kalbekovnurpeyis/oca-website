import { tns } from "tiny-slider/src/tiny-slider";
import OverlayScrollbars from "overlayscrollbars";
import GLightbox from "glightbox";
import * as basicLightbox from "basiclightbox";

import Loader from "../loader";

const controlsText = [
  '<svg width="12" height="19" viewBox="0 0 12 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.8296 17.8686L2.55273 9.59175L10.8296 1.24295" stroke="#000E26" stroke-opacity="0.5" stroke-width="3" stroke-miterlimit="10"/></svg>',
  '<svg width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.92239 1.88306L10.1992 10.1599L1.92239 18.5087" stroke="#000E26" stroke-opacity="0.5" stroke-width="3" stroke-miterlimit="10"/></svg>'
];

const stopVideo = () => {
  const videos = document.querySelectorAll("video");
  videos.forEach(video => {
    video.pause();
    video.currentTime = 0;
  });
};

// Show Loader
// Loader.showLoader();

document.addEventListener("DOMContentLoaded", () => {
  // Multimedia List Page SLider
  const sliderWrap = document.querySelector(".multimedia-slider");
  const sliderItems = document.querySelectorAll(".slider__item");

  if (sliderWrap && sliderItems.length) {
    const controleActiveSlides = slider => {

      setTimeout(() => {
        const sliderInfo = slider.getInfo();
        if (
          sliderWrap &&
          sliderInfo.index > 0 &&
          sliderInfo.index < sliderInfo.slideCountNew - 1
        ) {
          const currentSlide = sliderWrap.querySelector(".tns-slide-current");
          if (currentSlide) {
            currentSlide.classList.remove("tns-slide-current");
          }
          const activeSlides = sliderWrap.querySelectorAll(".tns-slide-active");
          activeSlides[1].classList.add("tns-slide-current");
        } else {
          controleActiveSlides(slider);
        }
      }, 10);
    };

    const slider = tns({
      container: ".multimedia-slider",
      autoWidth: true,
      items: 3,
      center: true,
      mouseDrag: true,
      speed: 500,
      controlsText: controlsText,
      nav: false,
      touch: true,
      preventScrollOnTouch: "auto",
      gutter: 10,
      lazyload: true,
      responsive: {
        426: {
          gutter: 40,
          items: 1
        },
        769: {
          gutter: 60
        },
        1025: {
          gutter: 100
        }
      },
      onInit: () => {
        controleActiveSlides(slider);

        // Hide Loader
        Loader.hideLoader();
      }
    });
    slider.events.on("indexChanged", () => controleActiveSlides(slider));
  } else {
    // Hide Loader
    Loader.hideLoader();
  }

  // --------------------------------------------------------------------------------

  // Multimedia Inner Page Big Slider
  const sliderWrapClasses = [
    {
      container: ".multimedia__photo-slider",
      nav: ".photo-slider__nav"
    },
    {
      container: ".multimedia__video-slider",
      nav: ".video-slider__nav"
    }
  ];

  sliderWrapClasses.map(function(sliderEl) {
    const innerSliderWrap = document.querySelector(sliderEl.container);
    if (innerSliderWrap) {
      [...document.querySelectorAll(`${sliderEl.container} .item__wrap`)].map(el => {
        const src = el.getAttribute("data-src");
        const type = el.getAttribute("data-type");
        let template = null;

        if (type && type !== '') {
          if (type === 'image') {
            template = `<img src="${src}">`;
          } else if (type === 'video') {
            template = `
            <video controls autoplay data-id="2">
              <source src="${src}" type="video/mp4">
            </video>
          `;
          } else if (type === "youtube") {
            template = `
            <iframe src="${src}?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%; height: 100%; max-width: 920px; max-height: 640px;"></iframe>
          `;
          }
        }

        if (template) {
          el.onclick = e => {
            basicLightbox
              .create(template)
              .show();
          };
        }
      });

      const slider = tns({
        container: sliderEl.container,
        items: 1,
        center: true,
        mouseDrag: true,
        speed: 1000,
        controlsText: controlsText,
        touch: true,
        preventScrollOnTouch: "auto",
        navContainer: `${sliderEl.nav} .custom-nav-wrap`,
        navAsThumbnails: true,
        lazyload: true,
        onInit: () => Loader.hideLoader()
      });
      slider.events.on("transitionStart", () => {
        scrollBar.scroll(
          {
            el: document.querySelector(`${sliderEl.nav} .tns-nav-active`),
            block: "center"
          },
          300
        );
        stopVideo();
      });

      // Multimedia Inner Nav Scrollbar
      const scrollElement = document.querySelector(sliderEl.nav);
      const scrollBar = OverlayScrollbars(scrollElement, {
        className: "os-theme-round-light"
      });
    } else {
      // Hide Loader
      Loader.hideLoader();
    }
  })
});
