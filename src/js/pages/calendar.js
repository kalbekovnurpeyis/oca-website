import noUiSlider from 'nouislider';
import wNumb from 'wnumb';
import { getQueryVariable } from '../helpers';

document.addEventListener('DOMContentLoaded', () => {
  const rangeFromInput = document.querySelector('.filter__year-from');
  const rangeToInput = document.querySelector('.filter__year-to');
  const handlesSlider = document.querySelector('.filter__year-range');
  const input = document.querySelector('.filter__keyword');
  const eventsList = document.querySelector('.events-list');

  const initialMinYear = (window.__rangeValues && window.__rangeValues.minYear) || 1980;
  const initialMaxYear = (window.__rangeValues && window.__rangeValues.maxYear) || new Date().getFullYear() + 12;
  const startYear = Number(getQueryVariable('year-from') || Math.max(initialMinYear, new Date().getFullYear() - 12));
  const endYear = Number(getQueryVariable('year-to') || new Date().getFullYear());
  const keyword = getQueryVariable('keyword') || '';

  const rangeValues = [initialMinYear, initialMaxYear];

  // Set initial values
  rangeFromInput.value = startYear;
  rangeToInput.value = endYear;
  input.value = keyword;

  noUiSlider
    .create(handlesSlider, {
      start: [startYear, endYear],
      step: 1,
      connect: true,
      tooltips: [true, wNumb({ decimals: 0 })],
      format: wNumb({ decimals: 0 }),
      range: {
        min: rangeValues[0],
        max: rangeValues[1],
      },
    })
    .on('change', ([min, max]) => {
      rangeFromInput.value = min;
      rangeToInput.value = max;
    });

  // Collapse description
  eventsList.addEventListener('click', e => {
    if (e.target.classList.contains('event__expand-btn')) {
      const content = e.target.parentNode.parentNode;
      content.classList.toggle('event__content--expanded');
    }
  });
});
