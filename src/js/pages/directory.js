import OverlayScrollbars from 'overlayscrollbars';

document.addEventListener('DOMContentLoaded', () => {
  // Scrollbar Library
  const scrollElement = document.querySelector('.games__categories');
  const scrollBar = OverlayScrollbars(scrollElement, {
    className: 'os-theme-round-light',
  });
  // Scroll List To Active Element
  scrollBar.scroll({
    el: document.querySelector('.games__category-link--active'),
    block: 'center',
  });
});
