import mixitup from 'mixitup';
import OverlayScrollbars from 'overlayscrollbars';
import { debounce, isIos, getIosVersion } from '../helpers';
import { createDesktopPhotoSlider } from '../desktop-photo-slider';
import photoSlider from '../photo-slider';
import Loader from '../loader';

document.addEventListener('DOMContentLoaded', () => {
  let currentMobileListEntry = 'A';
  const hasIntersectionObserver = 'IntersectionObserver' in window;
  const sportsList = document.querySelector('.sports-list');
  const sportsListMobile = document.querySelector('.sports-list-mobile');
  const sportsListMobileTooltip = document.querySelector(
    '.sports-list-mobile__tooltip'
  );
  const tabToggles = document.querySelector('.tab__toggles-list');
  const toggleCurrentEntryVisibility = debounce(visible => {
    sportsListMobileTooltip.classList[visible ? 'add' : 'remove']('visible');
  }, 200);

  // Set initial letter
  sportsListMobileTooltip.innerHTML = currentMobileListEntry;

  // Render features
  renderTabsAndSliders();
  createListOfDisciplinesMixitup();

  // Scrollbar Library
  OverlayScrollbars(tabToggles, {
    className: 'os-theme-light',
  });
  const sportsListDesktopScroll = OverlayScrollbars(sportsList, {
    className: 'os-theme-round-light',
    callbacks: {
      onInitialized() {
        sportsList.classList.add('visible')
      }
    }
  });
  const sportsListMobileScroll = OverlayScrollbars(sportsListMobile, {
    className: 'os-theme-round-light',
    callbacks: {
      onInitialized: () => {
        if (isIos()) {
          const root = document.getElementsByTagName('html')[0];
          const shouldOverflowScroll = getIosVersion() < 13;

          root.classList.add('ios');

          if (shouldOverflowScroll) {
            root.classList.add('overflow-scroll');
          }
        }
        if (hasIntersectionObserver) {
          // Observe mobile list items and set current visible entry
          const mobileSportsObserver = new window.IntersectionObserver(
            entries => {
              entries.forEach(entry => {
                if (entry.isIntersecting) {
                  const currentEntry = entry.target.dataset.letter;

                  if (currentEntry !== currentMobileListEntry) {
                    currentMobileListEntry = currentEntry;
                    sportsListMobileTooltip.innerHTML = currentMobileListEntry;
                  }
                }
              });
            },
            {
              root: sportsListMobile,
              rootMargin: '0px 20px 0px',
              threshold: 0.8,
            }
          );

          document
            .querySelectorAll('.sports-list-mobile__item')
            .forEach(el => mobileSportsObserver.observe(el));
        }
      },
      // Toggle current entry tooltip visibility
      onScrollStart: () =>
        hasIntersectionObserver && toggleCurrentEntryVisibility(true),
      onScrollStop: () =>
        hasIntersectionObserver && toggleCurrentEntryVisibility(false),
    },
  });

  // Scroll List To Active Element
  sportsListDesktopScroll.scroll({
    el: document.querySelector('.list-item--active'),
    block: 'center',
  });

  sportsListMobileScroll.scroll({
    el: document.querySelector('.sports-list-mobile__item-wrapper.selected'),
    block: 'center',
  });
  sportsListMobileScroll.getElements().scrollbarHorizontal.handle.style.width =
    '10%';

  const relatedLinks = document.querySelectorAll('.link__description');
  relatedLinks.forEach(link =>
    link.addEventListener('click', toggleVisibility)
  );

  function toggleVisibility(e) {
    e.preventDefault();
    if (
      e.currentTarget.classList.contains('link__description') &&
      e.currentTarget.classList.contains('hidden')
    ) {
      e.currentTarget.classList.remove('hidden');
    } else {
      e.currentTarget.classList.add('hidden');
    }
  }

  // Fixed tabs on scroll
  const tabs = document.querySelector('.tabs');
  const tabsHeader = document.querySelector('.tabs__header');
  const tabContainer = tabsHeader.querySelector('.tab__toggles-list');

  function handleScroll() {
    const isTabsFixed = tabsHeader.classList.contains('fixed');
    const pageOffset = window.pageYOffset;
    const threshold = getOffset(tabs);

    if ((pageOffset > threshold) && !isTabsFixed) {
      tabs.setAttribute('style', 'padding-top: 78px');
      sportsList.parentNode.classList.add('fixed');
      tabsHeader.classList.add('fixed');
      tabContainer.classList.add('content');
    }
    if ((pageOffset <= threshold) && isTabsFixed) {
      sportsList.parentNode.classList.remove('fixed');
      tabsHeader.classList.remove('fixed');
      tabContainer.classList.remove('content');
      tabs.removeAttribute('style');
    }
  }
  document.addEventListener('scroll', handleScroll);

  function renderTabsAndSliders() {
    let desktopSlider = null;
    let mobileSlider = null;
    const desktopMQ = window.matchMedia('(min-width: 769px)');
    const opts = {
      callbacks: {
        onMixStart: (_, state) => {
          const isPhotosTab = state.show.some(node =>
            node.classList.contains('photos')
          );

          if (isPhotosTab && !desktopSlider) {
            desktopSlider = createDesktopPhotoSlider();
          }
        },
      },
    };
    let mixer = createTabSectionsMixitup(opts);

    function onMQChange(e) {
      if (e.matches) {
        mixer = createTabSectionsMixitup(opts);

        if (mobileSlider) {
          mobileSlider.destroy();
          mobileSlider = null;
        }
      } else {
        mixer.destroy(true);
        mixer = null;

        if (desktopSlider) {
          desktopSlider.destroy();
          desktopSlider = null;
        }

        if (!mobileSlider) {
          mobileSlider = photoSlider();
        }
      }
      Loader.hideLoader();
    }
    desktopMQ.addListener(onMQChange);
    onMQChange(desktopMQ);
  }
});

function createTabSectionsMixitup(options) {
  const activeTab = document.querySelector(".active-tab");
  const activeTabData = activeTab
    ? activeTab.getAttribute("data-filter")
    : ".about";
  
  return mixitup('.tabs__body', {
    animation: {
      duration: 200,
      effectsIn: 'fade translateY(100%)',
      effectsOut: 'fade translateX(100%)',
      clampWidth: false,
      nudge: false,
    },
    load: {
      filter: activeTabData,
    },
    selectors: {
      control: '.tab__toggle',
    },
    classNames: {
      block: '',
      elementFilter: 'tab__toggle',
      modifierActive: 'active',
      delineatorModifier: '--',
    },
    ...options,
  });
}

function createListOfDisciplinesMixitup() {
  let mixitupInstance = null;
  const activeDiscipline = document.querySelector(".active-discipline");
  const firstItemId = document.querySelector(".disciplines__list-item");
  const filterId = activeDiscipline ? activeDiscipline : firstItemId;

  mixitupInstance = mixitup(".disciplines__item-container", {
    animation: {
      duration: 200,
      effects: "fade translateY(100%)",
      nudge: false
    },
    load: {
      filter: filterId.getAttribute("data-filter")
    },
    selectors: {
      target: ".disciplines__item-content",
      control: ".disciplines__list-item"
    },
    classNames: {
      block: "disciplines",
      elementFilter: "list-item",
      modifierActive: "active",
      delineatorModifier: "--",
      delineatorElement: "__"
    }
  });

  return mixitupInstance;
}

function getOffset(element, horizontal = false) {
  if(!element) return 0;

  return getOffset(element.offsetParent, horizontal) + (horizontal ? element.offsetLeft : element.offsetTop);
}