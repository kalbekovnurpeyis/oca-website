import GLightbox from 'glightbox';

document.addEventListener('DOMContentLoaded', () => {
  GLightbox({
    selector: 'mark-wrap',
    moreLength: 0,
    loop: true,
  });
});
