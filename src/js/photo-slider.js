import { tns } from 'tiny-slider/src/tiny-slider';
import Loader from './loader';

function controlActiveSlides(slider, container) {
  setTimeout(() => {
    const currentSlide = container.querySelector(".tns-slide-current");
    if (currentSlide) {
      currentSlide.classList.remove("tns-slide-current");
    }
    const activeSlides = container.querySelectorAll(".tns-slide-active");
    activeSlides[0].onclick = () => slider.goTo("prev");
    activeSlides[2].onclick = () => slider.goTo("next");
    activeSlides[1].classList.add("tns-slide-current");
    activeSlides[1].onclick = false;
  }, 10);
}

// Gallery Slider
const photoSlider = () => {
  const sliderContainer = document.querySelector('.photo-slider');
  if (sliderContainer) {
    const slider = tns({
      container: sliderContainer,
      autoWidth: true,
      items: 3,
      center: true,
      mouseDrag: true,
      lazyload: true,
      speed: 1000,
      controls: false,
      nav: false,
      touch: true,
      preventScrollOnTouch: 'auto',
      onInit: () => {
        controlActiveSlides(slider, sliderContainer);
        Loader.hideLoader();
      },
    });
    slider.events.on('transitionStart', () => controlActiveSlides(slider, sliderContainer));

    return slider;
  } else {
    Loader.hideLoader();
  }
};

export default photoSlider;
