const loaderWrap = document.querySelector('.loader__wrap');
const Loader = {
  hideLoader: () => {
    if (document.body.classList.contains('block-scroll')) {
      document.body.classList.remove('block-scroll');
    }
    if (loaderWrap) {
      loaderWrap.classList.add('loader__hidden');
    }
  },
  showLoader: () => {
    if (loaderWrap) {
      loaderWrap.classList.remove('loader__hidden');
    }
  },
};
export default Loader;
