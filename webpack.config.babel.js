import 'core-js/stable';
import 'regenerator-runtime/runtime';

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const camelCase = require('camelcase');
const { getEntries } = require('./utils');

function generateHtmlPlugins(templateDir) {
  const entries = getEntries(templateDir, 'pug');

  return Object.keys(entries).map(filename =>
    new HtmlWebpackPlugin({
      filename: `${filename}.html`,
      template: entries[filename],
      inject: false,
      hash: true,
      title: filename
    })
  );
}

const pugFiles = generateHtmlPlugins('./src/pug/pages');

const entries = {
  // Styles
  'commonStyle': './src/sass/common.sass',
  ...getEntries('./src/sass/pages', 'sass', key => camelCase(`${key}-style`)),
  // Scripts
  ...getEntries('./src/js/pages', 'js'),
}

module.exports = (env, argv) => ({
  entry: entries,
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].[hash].js',
  },
  devtool: argv.mode === 'development' ? 'cheap-module-eval-source-map' : 'source-map',
  devServer: {
    liveReload: true,
    open: true,
    compress: true,
    disableHostCheck: true,
    host: '0.0.0.0',
    port: 9000,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '../',
            name: 'images/[name].[ext]',
          },
        },
      },
      {
        test: /\.(mov|mp4)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: '../',
              name: 'videos/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '../',
            name: 'fonts/[name].[ext]',
          },
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        include: path.resolve(__dirname, 'src/sass'),
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              config: {
                path: 'postcss.config.js',
              },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.pug$/,
        loader: 'pug-loader',
        options: {
          pretty: true,
        },
      },
    ],
  },
  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin({}),
      new UglifyJsPlugin(),
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename:
        argv.mode === 'development'
          ? 'styles/[name].css'
          : 'styles/[name].[hash].css',
      chunkFilename:
        argv.mode === 'development' ? 'styles/[id].css' : 'styles/[id].[hash].css',
    }),
    new WebpackMd5Hash(),
    ...pugFiles,
  ],
});
