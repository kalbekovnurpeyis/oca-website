const glob = require('glob');

export function getEntries(context, extension, keyFn = i => i) {
  if (context[context.length - 1] !== '/') {
    context += '/';
  }

  extension = `.${extension}`;

  const files = glob.sync(`${context}**/*${extension}`);
  const entries = {};

  files.forEach((file) => {
    const entryName = keyFn(file.replace(context, '').replace(extension, ''));

    entries[entryName] = file;
  });

  return entries;
};